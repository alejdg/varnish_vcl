vcl 4.0;
import directors;

backend web01 {
        .host = "XXX.XXX.XXX.XXX";
        .port = "80";
	.between_bytes_timeout = 200s;
        .first_byte_timeout = 300s;
        .probe = {
                .url = "/check.txt";
                .interval = 5s;
                .timeout = 1s;
                .window = 5;
                .threshold = 3;
        }
}

backend web02 {
        .host = "XXX.XXX.XXX.XXX";
        .port = "80";
	.between_bytes_timeout = 200s;
        .first_byte_timeout = 300s;
        .probe = {
                .url = "/check.txt";
                .interval = 5s;
                .timeout = 1s;
                .window = 5;
                .threshold = 3;
        }
}

backend web03 {
        .host = "XXX.XXX.XXX.XXX";
        .port = "80";
	.between_bytes_timeout = 200s;
        .first_byte_timeout = 300s;
        .probe = {
                .url = "/check.txt";
                .interval = 5s;
                .timeout = 1s;
                .window = 5;
                .threshold = 3;
        }
}

sub vcl_init {
	new pool = directors.round_robin();
	pool.add_backend(web01);
	pool.add_backend(web02);
	pool.add_backend(web03);
}
