vcl 4.0;
import std;

include "backends.vcl";

#Acl contendo os ips permitidos para a visualizacao dos cabecalhos personalizados
acl trusted {
	"localhost";
	"XXX.XXX.XXX.XXX";
}

#Acl para liberacao de purge
acl purge {
	"localhost";
	"XXX.XXX.XXX.XXX"/20;
	"XXX.XXX.XXX.XXX";
}

#Procedimento de tratamento de requisi��o
sub vcl_recv {

        #Seleciona o pool a ser utilizado
        set req.backend_hint = pool.backend();

        #Insere o ip do cliente nos logs do varnish
        #unset req.http.X-Forwarded-For;
        #set    req.http.X-Forwarded-For = client.ip;
	
	#Remocao do cache-control = 0
	unset req.http.Cache-Control;

	#Desabilita o grace por padrao a menos que seja sobreescrito
	set req.http.grace = "none";

	#Identificacao do metodo PURGE para limpeza de cache.
        if (req.method == "PURGE"){
                if (std.ip(req.http.X-Real-IP, client.ip) !~ purge){
                        return (synth(405, "Not allowed."));
                } else {
			if (req.http.X-host){
				if (req.http.X-url){
					ban ("req.http.host ~ " + req.http.X-host + "&& req.http.url ~ " + req.http.X-url);
					return(synth(200, "Ban Added - host && url"));
				}
				ban ("req.http.host ~ " + req.http.X-host);
				return(synth(200, "Ban Added - host"));
			} else {
				if (!req.http.X-url){
					return(synth(404, "Vazio"));
				}

				ban ("req.http.url ~ " + req.http.X-url);
				#Entrega uma pagina estatica para que a requisicao nao chegue ao backend.
				return(synth(200, "Ban Added!"));
			}
		}
        }

        #Caso a requisi��o n�o seja um GET e n�o possua Cabe�abecalho e executa um pass.
        if (req.method != "GET" && req.method != "HEAD") {
            return(pass);
        }

	#Se for um arquivo estatico, ele sera cacheado
	if (req.url ~ "\.(ico|png|gif|jpg|swf|css|js)") {
                return(hash);
        }

        #Urls que n�o passam pelo cache
        if (
            (req.url ~ "/teste.jpg") ||
            (req.url ~ "/no_cache") ||
            (req.url ~ "/testando/sem/cache)

        {
            return(pass);
        }

	#Regra para evitar que os cookies sejam recolocados apos a aplicao remove-los
        if (req.url ~ "/limpa_cookies.php") {
                return (pipe);
        }

	if (req.http.Cookie ~ "PHPSESSID=") {
                # A requisicao ja possui um session ID, entao guarde ela temporariamente
                set req.http.Tmp-Set-Cookie = req.http.Cookie;
        } 

        #Esse miss tornou-se desnecessario, pois o tracking da visita e feito no js do pageview
        else { 
                # A requisicao nao tem um session ID, entao force um miss
                return (pass);
        }
	
        #Caso a requisi��o seja um POST ou um GET verifica o cache
        if ((req.method == "POST" || req.method == "GET")){
             return(hash);
        }
}

sub vcl_hash {
	#O hash foi modificado para que cacheie imagens comuns na area de administracao.
        hash_data(req.url);
        if (req.url ~ "/adm/(.*)\.(ico|png|gif|jpg|swf|js)"){
                return (lookup);
        }
        if (req.http.host) {
                hash_data(req.http.host);
        } else {
                hash_data(server.ip);
        }
        return (lookup);
}

#Procedimento de acerto (cache hit)
sub vcl_hit {
	if (obj.ttl >= 0s) {
		# normal hit
		return (deliver);
	}
	# Objeto nao encontrado no cache. O conteudo "stale" sera verificado.
	if (std.healthy(req.backend_hint)) {
		# Se o backend esta saudavel a idade maxima do objeto nao deve ser maior que 10s.
		if (obj.ttl + 10s > 0s) {
			set req.http.grace = "normal(limited)";
			return (deliver);
		} else {
			# Nenhum objeto em foi encontrado. O grace nao foi utilizado e o objeto foi buscado diretamente do backend.
			return(fetch);
		}
	} else {
		# O backend esta "doente", utilize o grace "full", ou seja, pega um conteudo do cache mesmo se ele estiver "stale"
		if (obj.ttl + obj.grace > 0s) {
			set req.http.grace = "full";
			return (deliver);
		} else {
			# Nenhum objeto no estado "grace" foi encontrado.
			return (fetch);
		}
	}
}

#Procedimento de erro (cache miss)
sub vcl_miss {
        #Caso a requisi��o seja PURGE exibe o erro abaixo
        if (req.method == "PURGE") {
		return (synth(404, "Not in cache"));
        } else {
                return(fetch);
        }
}


#Procedimento de tratamento do erro do varnish
sub vcl_backend_error {
        #Em caso de erro exibe a mensagem de erro informada pelo backend
        set beresp.http.Content-Type = "text/html; charset=utf-8";
        synthetic ("<html><head><title>" + beresp.status + " </title></head><body><h1>Error " + beresp.status + " </h1><p>XID: " + bereq.xid + "</p></body></html>");
	return (deliver);
}

#Procedimento de coleta na origem
sub vcl_backend_response {

	#Caso o ip de acesso seja um dos abaixo... 
        if (bereq.http.X-Forwarded-For == "201.62.66.162"){
		#Indica a origem do objeto
		set beresp.http.X-Server-Name = beresp.backend.name;
		set beresp.http.X-Server-IP = beresp.backend.ip;
	}
	
	#Se a resposta tiver Set-Cookie ...
        if (beresp.http.Set-Cookie) {
                # ... entao, armazena o set-cookie momentaneamente
                set beresp.http.Tmp-Set-Cookie = beresp.http.Set-Cookie;
                # ... e remove esse header
                unset beresp.http.Set-Cookie;
        }

	#ttl minimo para que o grace funcione mesmo em paginas nao cacheadas
	set beresp.ttl = 10s;
	#ativacao do grace para 1h para o caso de instabilidades
	set beresp.grace = 1h;

	if (beresp.ttl > 0s){

		#Caso a requisicao seja um POST ou um GET
		if ((bereq.method == "POST" || bereq.method == "GET")){
			#Remocao de cabecalhos indesejados na resposta
			unset beresp.http.expires;
			unset beresp.http.pragma;

			#Caso seja um arquivo estatico, define um ttl de ~1 ano
			if (bereq.url ~ "\.(ico|png|gif|jpg|swf|css|js)"){
				set beresp.http.Cache-Control = "max-age=32400000";
				set beresp.ttl = 9000d;
			} else {
				set beresp.http.Cache-Control = "max-age=7200";
				set beresp.ttl = 1h;
			}
		}
	}

        #Caso seja informado o status 301(Moved Permanently) pelo servidor, gera um cache de 24h
        if (beresp.status == 301) {
            set beresp.ttl = 24h;
        }
        #Caso seja informado o status 404(Not Found) pelo servidor, gera um cache de 1h
        #if (beresp.status == 404) {
        #    set beresp.ttl = 1h;
        #}

        #Caso ocorra o erro 404, um novo backend sera solicitado
        if (beresp.status == 404) {
                if ((bereq.retries > 7)){
                        set beresp.ttl = 1h;
                } else {
                        return(retry);
                }
        }
		
		##Saintmode ainda nao funciona na versao 4, utilizando o grace para fazer a mesma funcao
        #Caso seja informado pelo servidor qualquer um dos status de erro abaixo, faz com que o objeto seja verificado em outro backend.
        #Se mesmo assim n�o estiver dispon�vel, uma vers�o em cache � entregue.
        #if (beresp.status == 403 || beresp.status == 500 || beresp.status == 501 || beresp.status == 502 || beresp.status == 503 || beresp.status == 504) {
        #    set beresp.saintmode = 10s;
        #    return(restart);
        #}
	
}

#Procedimento de entrega
sub vcl_deliver {
        #Insere o grace da requisicao na resposta
        set resp.http.grace = req.http.grace;

	# Insere o Cookie caso seja enviado pelo backend (nao insere caso o cliente ja possua)
        if (resp.http.Tmp-Set-Cookie && !req.http.Tmp-Set-Cookie) {
                set resp.http.Set-Cookie = resp.http.Tmp-Set-Cookie;
                # E remove o Cookie tmp
                unset resp.http.Tmp-Set-Cookie;
        #Limpa a variavel temporaria do cookie caso o cliente ja o possua
        } elseif (req.http.Tmp-Set-Cookie){
                unset resp.http.Tmp-Set-Cookie;
        }

        #Caso o ip de acesso esteja na ACL "trusted" os cabecalhos abaixo serao inseridos
        if (client.ip ~ trusted){
                #Insere o nivel de grace utilizado na requisicao
                set resp.http.grace = req.http.grace;

                #Indica se o objeto teve "hit" ou "miss"
                if (obj.hits > 0) {
                                set resp.http.X-Cache = "HIT";
                } else {
                                set resp.http.X-Cache = "MISS";
                }

        } else{
                unset resp.http.grace;
                unset resp.http.Via;
                unset resp.http.x-varnish;
        }
}
